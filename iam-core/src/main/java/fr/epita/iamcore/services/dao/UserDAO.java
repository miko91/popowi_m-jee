package fr.epita.iamcore.services.dao;

import fr.epita.iamcore.datamodel.User;

/**
 * User DAO
 * Provide methods to manage users
 *
 */
public interface UserDAO extends DAO<User> {
	/**
	 * Get a user with its username and password for authentication
	 * @param username String the user's username
	 * @param password String user's password
	 * @return User the authenticated user or null
	 */
	User authenticate(String username, String password);
}
