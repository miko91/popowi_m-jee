package fr.epita.iamcore.services.dao.impl;

import java.util.Collection;

import javax.inject.Inject;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import fr.epita.iamcore.datamodel.Identity;
import fr.epita.iamcore.services.dao.IdentityDAO;

/**
 * JPA implementation of the IdentityDAO
 *
 */
public class IdentityJPADAO implements IdentityDAO {

	@Inject
	private SessionFactory sf;

	/**
	 * Get the session factory
	 * @return SessionFactory sf
	 */
	public final SessionFactory getSf() {
		return sf;
	}

	/**
	 * Set the session factory
	 * @param sf
	 */
	public final void setSf(SessionFactory sf) {
		this.sf = sf;
	}

	/**
	 * @see fr.epita.iamcore.services.dao.DAO#save(java.lang.Object)
	 */
	public void save(Identity id) {
		Session session = sf.openSession();
		session.saveOrUpdate(id);
		session.flush();
		session.close();

	}

	/**
	 * @see fr.epita.iamcore.services.dao.DAO#update(java.lang.Object)
	 */
	public void update(Identity id) {
		Session session = sf.openSession();
		session.saveOrUpdate(id);
		session.flush();
		session.close();
	}

	/**
	 * @see fr.epita.iamcore.services.dao.DAO#delete(java.lang.Object)
	 */
	public void delete(Identity id) {
		Session session = sf.openSession();
		session.delete(id);
		session.flush();
		session.close();
	}

	/**
	 * @see fr.epita.iamcore.services.dao.DAO#getById(java.lang.Object)
	 */
	@Override
	public Identity getById(Object id) {
		Session session = sf.openSession();
		Criteria crits = session.createCriteria(Identity.class)
				.add(Restrictions.eq("id", id));
		return (Identity) crits.uniqueResult();
	}


	/**
	 * @see fr.epita.iamcore.services.dao.DAO#search(Collection<Criterion> restrictions)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<Identity> search(Collection<Criterion> restrictions) {
		Session session = sf.openSession();
		Criteria crits = session.createCriteria(Identity.class);
		for (Criterion r : restrictions) {
			crits.add(r);
		}
		return crits.list();
	}
	/**
	 * @see fr.epita.iamcore.services.dao.DAO#getCount()
	 */
	@Override
	public int getCount() {
		Session session = sf.openSession();
		return (int) session.createQuery("count(*) FROM IDENTITIES").uniqueResult();
	}
}
