package fr.epita.iamcore.services.dao;

import java.util.Collection;
import org.hibernate.criterion.Criterion;

public interface DAO<T> {
	/**
	 * Save the entity
	 * @param id
	 */
	void save(T id);
	
	/**
	 * Update the entity
	 * @param id
	 */
	void update(T id);
	
	/**
	 * Delete the entity
	 * @param id
	 */
	void delete(T id);
	
	/**
	 * Get the entity by id
	 * @param id
	 * @return
	 */
	T getById(Object id);
	
	/**
	 * Search entities with a list of Criterion
	 * @param restrictions
	 * @return
	 */
	Collection<T> search(Collection<Criterion> restrictions);
	
	/**
	 * Get the count of entities
	 * @return
	 */
	int getCount();
}
