/**
 *
 */
package fr.epita.iamcore.services.dao;

import fr.epita.iamcore.datamodel.Identity;

/**
 * Identity DAO
 * Provide methods to manage identities
 */
public interface IdentityDAO extends DAO<Identity> {
}
