package fr.epita.iam.iamcore.tests;

import java.util.ArrayList;
import java.util.Collection;

import javax.inject.Inject;

import org.junit.Test;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.epita.iam.iamlog.IamLogManager;
import fr.epita.iam.iamlog.IamLogger;
import fr.epita.iamcore.datamodel.User;
import fr.epita.iamcore.services.dao.UserDAO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"/applicationContext.xml"})
public class TestUserDAO {
	private static IamLogger LOGGER = IamLogManager.getIamLogger(TestUserDAO.class);
	
	@Inject
	private UserDAO dao;
	
	@Test
	public void TestSave() {
		// Given
		User toSave = new User("login", "password");
		Assert.assertNull(toSave.getId());
		
		// When
		dao.save(toSave);
		
		// Then
		Long id = toSave.getId();
		Assert.assertNotNull(id);
		LOGGER.info(toSave);
	}
	
	@Test
	public void TestUpdate() {
		// Given
		User toUpdate = new User("badLogin", "password");
		Assert.assertNull(toUpdate.getId());
		LOGGER.info("Insert new user");
		dao.save(toUpdate);
		Long id = toUpdate.getId();
		Assert.assertNotNull(id);
		
		// When
		toUpdate.setUsername("goodLogin");
		LOGGER.info(String.format("Update user %d", id));
		dao.update(toUpdate);
		
		// Then
		LOGGER.info("Try to get the user by id");
		User result = dao.getById(id);
		Assert.assertEquals(toUpdate.getUsername(), result.getUsername());
	}
	
	@Test
	public void TestDelete() {
		// Given
		User toDelete = new User("toDelete", "password");
		Assert.assertNull(toDelete.getId());
		dao.save(toDelete);
		Long id = toDelete.getId();
		Assert.assertNotNull(id);
		
		// When
		dao.delete(toDelete);
		
		// Then
		User result = dao.getById(id);
		Assert.assertNull(result);
	}
	
	@Test
	public void TestGetById() {
		// Given
		User toCreate = new User("login", "pass");
		Assert.assertNull(toCreate.getId());
		dao.save(toCreate);
		Long id = toCreate.getId();
		Assert.assertNotNull(id);
		
		// When
		User result = dao.getById(id);
		
		// Then
		Assert.assertNotNull(result);
		Assert.assertEquals(toCreate, result);
	}
	
	@Test
	public void TestSearch() {
		// Given
		User u1 = new User("login1", "UltraVeryLongPassword");
		dao.save(u1);
		Assert.assertNotNull(u1.getId());
		User u2 = new User("login2", "UltraVeryLongPassword");
		dao.save(u2);
		Assert.assertNotNull(u2.getId());
		User u3 = new User("login3", "UltraVeryLongPassword");
		dao.save(u3);
		Assert.assertNotNull(u3.getId());
		User u4 = new User("login4", "ShortPass");
		dao.save(u4);
		Assert.assertNotNull(u4.getId());
		
		// When
		Collection<User> all = dao.search(new ArrayList<Criterion>());
		
		// Then
		Assert.assertTrue(all.size() >= 4);
		
		// When
		Collection<Criterion> restrictions = new ArrayList<>();
		restrictions.add(Restrictions.eq("password", "UltraVeryLongPassword"));
		Collection<User> longPass = dao.search(restrictions);
		Assert.assertEquals(3, longPass.size());
		
		// When
		restrictions = new ArrayList<>();
		restrictions.add(Restrictions.eq("password", "ShortPass"));
		Collection<User> shortPass = dao.search(restrictions);
		Assert.assertEquals(1, shortPass.size());
	}
}
