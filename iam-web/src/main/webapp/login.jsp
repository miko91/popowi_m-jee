<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="en">
<head>
    <title>Login - IAM</title>
	
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    
	<link href="<c:out value="${pageContext.request.contextPath}" />/assets/css/bootstrap.css" rel="stylesheet" />
	<link href="<c:out value="${pageContext.request.contextPath}" />/assets/css/signin.css" rel="stylesheet" />
</head>
<body>
	<div class="container">
		<form class="form-signin" method="POST" action="<c:out value="${pageContext.request.contextPath}" />/login">
			<h2 class="form-signin-heading">IAM - Sign in</h2>
			<c:if test="${not empty sessionScope.message}">
				<div class="alert alert-danger alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<strong>Whoops !</strong> <c:out value="${sessionScope.message}" />
					<c:remove var="message" scope="session" />
				</div>
			</c:if>
			<label for="username" class="sr-only">Email address</label>
			<input type="test" id="username" name="username" class="form-control" placeholder="username" required autofocus>
			<label for="password" class="sr-only">Password</label>
			<input type="password" id="password" name="password" class="form-control" placeholder="Password" required>
			<button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
		</form>
	</div> <!-- /container -->

	<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/bootstrap.min.js"></script>	
</body>
</html>
