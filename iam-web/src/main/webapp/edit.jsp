<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<html lang="en">
<head>
	<title>Edit identity - IAM</title>
	
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    
	<link href="${pageContext.request.contextPath}/assets/css/bootstrap.css" rel="stylesheet" />
</head>
<body>
	<div class="container">
		<div class="jumbotron" style="margin-top: 20px!important;">
			<h1>Welcome to the IAM System</h1>
			<a href="${pageContext.request.contextPath}/logout">Logout</a>
		</div>
		<ol class="breadcrumb">
			<li><a href="${pageContext.request.contextPath}/">Home</a></li>
			<li><a href="${pageContext.request.contextPath}/search">Search</a></li>
			<li class="active">Edit identity</li>
		</ol>
		
		
		<div class="page-header">
			<h1 class="text-primary">Edit identity</h1>
		</div>
		<form method="post" class="form-horizontal" action="${pageContext.request.contextPath}/edit?id=${param.id}">
			<div class="form-group">
				<label for="firstName" class="col-sm-2 control-label">First
					Name</label>

				<div class="col-sm-10">
					<input type="text" class="form-control" name="firstName"
						id="firstName" placeholder="First Name" value="${identity.fname}" required autofocus/>
				</div>
			</div>
			<div class="form-group">
				<label for="lastName" class="col-sm-2 control-label">Last
					Name</label>

				<div class="col-sm-10">
					<input type="text" name="lastName" class="form-control"
						id="lastName" placeholder="Last Name" value="${identity.lname}" required />
				</div>
			</div>
			<div class="form-group">
				<label for="email" class="col-sm-2 control-label">Email</label>

				<div class="col-sm-10">
					<input type="email" name="email" class="form-control" id="email"
						placeholder="Email" value="${identity.email}" disabled />
				</div>
			</div>

			<div class="form-group">
				<label for="date" class="col-sm-2 control-label">Birth Date</label>

				<div class="col-sm-10">
					<input type="date" name="date" class="form-control" id="date"
						placeholder="Birth Date" value="${identity.getFormBirthday()}" required />
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<a class="btn btn-default" href="${pageContext.request.contextPath}/search">Cancel</a>
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</div>
		</form>
	</div>
</body>
</html>