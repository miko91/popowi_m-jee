<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="en">
<head>
	<title>IAM</title>
	
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    
	<link href="<c:out value="${pageContext.request.contextPath}" />/assets/css/bootstrap.css" rel="stylesheet" />
</head>
<body>
	<div class="container">
		<div class="jumbotron" style="margin-top: 20px!important;">
			<h1>Welcome to the IAM System</h1>
			<a href="${pageContext.request.contextPath}/logout">Logout</a>
		</div>
		<ol class="breadcrumb">
			<li class="active">Home</li>
		</ol>
		
		<c:if test="${not empty sessionScope.message}">
			<div class="alert alert-success alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<strong>Well !</strong> <c:out value="${sessionScope.message}" />
				<c:remove var="message" scope="session" />
			</div>
		</c:if>
		
		<div class="row">
			<div class="col-md-6">
				<h4>Identity Creation</h4>
				<p>Thanks to this action, you can create a brand new Identity, you can click on the button below to begin</p>
				<a class="btn btn-default" href="${pageContext.request.contextPath}/create">Create !</a>
			</div>
			<div class="col-md-6">
				<h4>Identity Search</h4>
				<p>Thanks to this action, you can search an identity and then access to its information. Through this action, you can also modify or delete the wished identity</p>
				<a class="btn btn-default" href="${pageContext.request.contextPath}/search">Search !</a>
			</div>
		</div>
	</div>
	<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/bootstrap.min.js"></script>
</body>
</html>