<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="en">
<head>
	<title>Search - IAM</title>
	
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    
	<link href="${pageContext.request.contextPath}/assets/css/bootstrap.css" rel="stylesheet" />
</head>
<body>
	<div class="container">
		<div class="jumbotron" style="margin-top: 20px!important;">
			<h1>Welcome to the IAM System</h1>
			<a href="${pageContext.request.contextPath}/logout">Logout</a>
		</div>
		<ol class="breadcrumb">
			<li><a href="${pageContext.request.contextPath}/">Home</a></li>
			<li class="active">Search</li>
		</ol>
		
		<div class="page-header">
			<h1 class="text-primary">Search</h1>
		</div>
		<c:if test="${not empty sessionScope.message}">
			<div class="alert alert-success alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<strong>Well !</strong> <c:out value="${sessionScope.message}" />
				<c:remove var="message" scope="session" />
			</div>
		</c:if>
		<div class="row">
			<div class="col-md-3">
				<form method="post" class="form-horizontal" action="${pageContext.request.contextPath}/search">
					<div class="form-group">
						<label for="firstName" class="col-sm-2 control-label">First
							Name</label>
		
						<div class="col-sm-10">
							<input type="text" class="form-control" name="firstName"
								id="firstName" placeholder="First Name" value="${firstName}" autofocus/>
						</div>
					</div>
					<div class="form-group">
						<label for="lastName" class="col-sm-2 control-label">Last
							Name</label>
		
						<div class="col-sm-10">
							<input type="text" name="lastName" class="form-control"
								id="lastName" placeholder="Last Name" value="${lastName}" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" class="btn btn-primary">Search</button>
						</div>
					</div>
				</form>
			</div>
			<div class="col-md-8">
				<c:if test="${ not empty identities }">
				<div class="text-right">
					<button class="btn btn-primary" type="button" id="edit-btn" disabled="disabled">Edit</button>
					<button class="btn btn-danger" type="button" id="delete-btn" disabled="disabled" data-toggle="modal" data-target="#deleteModal">Delete</button>
				</div>
				<table class="table">
					<thead>
						<tr>
							<th>Selection</th>
							<th>UID</th>
							<th>First name</th>
							<th>Last name</th>
							<th>Email</th>
							<th>Birthday</th>
						</tr>
					</thead>
					<tbody>
					<c:forEach var="identity" items="${identities}">
						<tr>
							<td><input name="selection" type="radio" data-id="${identity.id}" data-fname="${identity.fname}" data-lname="${identity.lname}" /></td>
							<td>${identity.id}</td>
							<td>${identity.fname}</td>
							<td>${identity.lname}</td>
							<td>${identity.email}</td>
							<td>${identity.getSearchBirthday()}</td>
						</tr>
					</c:forEach>
					</tbody>
				</table>
				</c:if>
			</div>
		</div>
	</div>
	<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="deleteModalLabel">Delete identity</h4>
	      </div>
	      <div class="modal-body">
	        <form id="delete-form">
	          <div class="form-group">
	            <p>
	            	Are you sure you want to delete the identity : <span class="text-danger" id="delete-name"></span>
	            </p>
	            <input type="hidden" id="delete-id" />
	          </div>
	        </form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="button" class="btn btn-danger" id="confirm-btn">Confirm</button>
	      </div>
	    </div>
	  </div>
	</div>
	<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/bootstrap.min.js"></script>
	<script type="text/javascript">
		$(function() {
			$('input[name="selection"]').on('change', function() {
				if ($(this).is(':checked')) {
					if ($('#edit-btn').is(':disabled')) {
						console.log('Enable edit/delete buttons');
						$('#edit-btn').prop('disabled', false);
						$('#delete-btn').prop('disabled', false);
					}
				}
				$('#edit-btn').on('click', function() {
					var id = $('input[name="selection"]:checked').data('id');
					console.log('Id is ' + id);
					if (id)
						window.location.replace('${pageContext.request.contextPath}/edit?id='+id);
				});
				$('#deleteModal').on('show.bs.modal', function() {
					var input = $('input[name="selection"]:checked');
					var modal = $(this);
					
					modal.find('#delete-name').html(input.data('fname') + ' ' + input.data('lname'));
					modal.find('#delete-id').val(input.data('id'));
				});
				$('#confirm-btn').on('click', function() {
					$('#deleteModal').modal('hide');
					var $id = $('input[name="selection"]:checked').data('id');
					$.ajax({
						url: "${pageContext.request.contextPath}/delete",
						type: "delete",
						dataType: "json",
						data: '{"id": "'+$id+'"}'
					}).done(function (data, textStatus, jqHXR) {
						if (data.status == true) {
							location.reload();
						} else {
							var $html = '<div class="alert alert-danger alert-dismissible" role="alert">' +
											'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
											'<strong>Whoops !</strong> ' + data.message +
										'</div>';
							$('div.page-header').after($html);
						}
					}).fail(function (jqXHR, textStatus, errorThrown) {
						console.log(textStatus);
                        console.log(errorThrown);
					});
				});
			});
		});
	</script>
</body>
</html>