package fr.epita.iam.web.actions;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import fr.epita.iam.iamlog.IamLogManager;
import fr.epita.iam.iamlog.IamLogger;

/**
 * Search handles identity search
 */
public class Search extends AbstractServlet {
	private static final long serialVersionUID = -2558307363146448037L;
	@SuppressWarnings("unused")
	private static IamLogger LOGGER = IamLogManager.getIamLogger(Search.class);

	/**
	 * Default constructor
	 */
	public Search() {}

	/**
	 * Handles GET method, return search.jsp witch an identity list.
	 * If the list of identities does not exists in the session, it looks for all identities.
	 * @see HttpServlet#doGet(HttpServletRequest req, HttpServletResponse resp)
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if (req.getAttribute("identities") == null) {
			ArrayList<Criterion> restrictions = new ArrayList<>();
			req.setAttribute("identities", dao.search(restrictions));
		}
		this.getServletContext().getRequestDispatcher("/search.jsp").forward(req, resp);
	}

	/**
	 * Handles POST method. Looks for identities with request parameters. Then return the doGet method.
	 * @see HttpServlet#doPost(HttpServletRequest req, HttpServletResponse resp)
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		ArrayList<Criterion> restrictions = new ArrayList<>();
		String fname = req.getParameter("firstName");
		String lname = req.getParameter("lastName");
		if (fname != null && !fname.equals("")) {
			restrictions.add(Restrictions.like("fname", "%"+fname+"%"));
			req.setAttribute("firstName", fname);
		}
		if (lname != null && !lname.equals("")) {
			restrictions.add(Restrictions.like("lname", "%"+lname+"%"));
			req.setAttribute("lastName", lname);
		}
		req.setAttribute("identities", dao.search(restrictions));
		doGet(req, resp);
	}
}
