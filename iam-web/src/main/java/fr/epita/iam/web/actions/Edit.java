package fr.epita.iam.web.actions;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.epita.iam.iamlog.IamLogManager;
import fr.epita.iam.iamlog.IamLogger;
import fr.epita.iamcore.datamodel.Identity;

/**
 * Edit class handle /edit with GET and POST methods
 * It's about Identity update
 */
public class Edit extends AbstractServlet {
	private static final long serialVersionUID = 9033490788771742969L;
	private static IamLogger LOGGER = IamLogManager.getIamLogger(Edit.class);

	/**
	 * Default constructor
	 */
	public Edit() {}

	/**
	 * Handle GET method. Return the edit.jsp
	 * @see HttpServlet#doGet(HttpServletRequest req, HttpServletResponse resp)
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			Identity identity = this.getIdentity(req.getParameter("id"));
			req.setAttribute("identity", identity);
			this.getServletContext().getRequestDispatcher("/edit.jsp").forward(req, resp);
		} catch (ParameterException e) {
			LOGGER.error(e.getMessage());
			this.getServletContext().getRequestDispatcher("/errors/404.jsp").forward(req, resp);
		}
	}

	/**
	 * Handle POST method. Update an identity with request parameters and return the search page
	 * @see HttpServlet#doPost(HttpServletRequest req, HttpServletResponse resp)
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			Identity identity = this.getIdentity(req.getParameter("id"));
			identity.setFname(req.getParameter("firstName"));
			identity.setLname(req.getParameter("lastName"));
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date date = null;
			try {
				date = sdf.parse(req.getParameter("date"));
				identity.setBirthday(date);
			} catch (ParseException e) {
				LOGGER.error("Unable to get the birthday : " + e.getMessage());
			}
			dao.update(identity);

			HttpSession session = req.getSession();
			session.setAttribute("message", "Entity successfully updated");
			resp.sendRedirect(req.getContextPath()+"/search");
		} catch (ParameterException e) {
			// TODO: handle exception
		}
	}
}
