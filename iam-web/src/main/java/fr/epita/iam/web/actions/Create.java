package fr.epita.iam.web.actions;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.epita.iam.iamlog.IamLogManager;
import fr.epita.iam.iamlog.IamLogger;
import fr.epita.iamcore.datamodel.Identity;

/**
 * Create class handle /create with GET and POST methods
 * It's about Identity creation
 *
 */
public class Create extends AbstractServlet {
	private static final long serialVersionUID = 1L;
	private static IamLogger LOGGER = IamLogManager.getIamLogger(Create.class);

    /**
     * Default constructor.
     */
    public Create() {}

    /**
     * Handle GET method. Return the create.jsp
	 * @see HttpServlet#doGet(HttpServletRequest req, HttpServletResponse resp)
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/create.jsp").forward(req, resp);
	}

	/**
	 * Handle POST method. Create an identity with request parameters and return the home page
	 * @see HttpServlet#doPost(HttpServletRequest req, HttpServletResponse resp)
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String firstName = req.getParameter("firstName");
		String lastName = req.getParameter("lastName");
		String email = req.getParameter("email");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		try {
			date = sdf.parse(req.getParameter("date"));
		} catch (ParseException e) {
			LOGGER.error("Unable to get the birthday : " + e.getMessage());
		}
		Identity identity = new Identity(firstName, lastName, email, date);
		LOGGER.info(identity);
		dao.save(identity);

		HttpSession session = req.getSession();
		session.setAttribute("message", "Entity successfully created");
		resp.sendRedirect(req.getContextPath());
	}
}
