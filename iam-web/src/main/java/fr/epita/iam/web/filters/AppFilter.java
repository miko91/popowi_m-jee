package fr.epita.iam.web.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.epita.iam.iamlog.IamLogManager;
import fr.epita.iam.iamlog.IamLogger;

/**
 * AppFilter handles app filters especially for the user connection.
 * It forbids page when the user is not authenticated. It always allows /login page and assets
 * @author Miko
 */
public class AppFilter implements Filter {
	private static IamLogger LOGGER = IamLogManager.getIamLogger(AppFilter.class);
	private static String SESSION = "userSession";
	
	/**
	 * @see javax.servlet.Filter#destroy()
	 */
	@Override
	public void destroy() {}

	/**
	 * Create filters for the app.
	 * @see javax.servlet.Filter#doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain arg2)
	 */
	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain arg2)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) arg0;
		HttpServletResponse resp = (HttpServletResponse) arg1;
		HttpSession session = req.getSession();
		
		String path = req.getRequestURI().substring(req.getContextPath().length());
		LOGGER.info(path);
		if (path.startsWith("/login") || path.startsWith("/assets")) {
			LOGGER.info("Filter");
			arg2.doFilter(req, resp);
		}
		else {
			if (session.getAttribute(SESSION) == null) {
				LOGGER.info("Redirect");
				resp.sendRedirect(req.getContextPath() + "/login");
			} else {
				LOGGER.info("Filter auth");
				arg2.doFilter(req, resp);
			}
		}		
	}

	/**
	 * Filter initialization
	 */
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {}
}
