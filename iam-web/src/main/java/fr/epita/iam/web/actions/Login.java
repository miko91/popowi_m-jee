package fr.epita.iam.web.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.epita.iam.iamlog.IamLogManager;
import fr.epita.iam.iamlog.IamLogger;
import fr.epita.iamcore.datamodel.User;

/**
 * Login handles the login mechanism
 */
public class Login extends AbstractServlet {
	private static final long serialVersionUID = 6145054221113555498L;
	@SuppressWarnings("unused")
	private static IamLogger LOGGER = IamLogManager.getIamLogger(Login.class);

	/**
	 * Default constructor
	 */
	public Login() {}

	/**
	 * Handle GET method. Return the login.jsp
	 * @see HttpServlet#doGet(HttpServletRequest req, HttpServletResponse resp)
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getServletContext().getRequestDispatcher("/login.jsp").forward(req, resp);
	}

	/**
	 * Handle POST method. Connect user with request parameters.
	 * If user is connected, set the session with the user and redirect to the home page
	 * @see HttpServlet#doPost(HttpServletRequest req, HttpServletResponse resp)
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		User user = userDao.authenticate(req.getParameter("username"), req.getParameter("password"));
		if (user != null) {
			req.getSession().setAttribute("userSession", user);
			resp.sendRedirect(req.getContextPath());
		} else {
			req.getSession().setAttribute("message", "Unable to login with these credentials");
			doGet(req, resp);
		}
	}
}
