package fr.epita.iam.web.actions;

import java.io.BufferedReader;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import fr.epita.iam.iamlog.IamLogManager;
import fr.epita.iam.iamlog.IamLogger;
import fr.epita.iamcore.datamodel.Identity;

/**
 * Delete class handle /delete with DELETE method
 * It's about Identity deletion
 *
 */
public class Delete extends AbstractServlet {
	private static final long serialVersionUID = -6664301205760084808L;
	@SuppressWarnings("unused")
	private static IamLogger LOGGER = IamLogManager.getIamLogger(Delete.class);

	/**
	 * Default constructor
	 */
	public Delete() {}

	/**
	 * Delete an identity with the given id in request
	 * @see HttpServlet#doDelete(HttpServletRequest req, HttpServletResponse resp)
	 */
	@Override
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		StringBuilder sb = new StringBuilder();
		BufferedReader buffer = req.getReader();
		String line = null;
		while ((line = buffer.readLine()) != null)
			sb.append(line);
		Gson gson = new Gson();
		JsonObject json = gson.fromJson(sb.toString(), JsonObject.class);
		Long id = json.get("id").getAsLong();
		Identity toDelete = dao.getById(id);
		JsonObject response = new JsonObject();
		if (toDelete != null) {
			dao.delete(toDelete);
			response.addProperty("status", true);
			HttpSession session = req.getSession();
			session.setAttribute("message", "Entity successfully deleted");
		} else {
			response.addProperty("status", false);
			response.addProperty("message", "Identity not found");
		}
		resp.getWriter().append(response.toString());
	}
}
