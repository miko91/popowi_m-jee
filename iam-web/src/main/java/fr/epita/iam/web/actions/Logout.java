package fr.epita.iam.web.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import fr.epita.iam.iamlog.IamLogManager;
import fr.epita.iam.iamlog.IamLogger;

/**
 * Logout handles the logout mechanism
 */
public class Logout extends AbstractServlet {
	private static final long serialVersionUID = -8293618991422159885L;
	@SuppressWarnings("unused")
	private static IamLogger LOGGER = IamLogManager.getIamLogger(Logout.class);

	/**
	 * Default constructor
	 */
	public Logout() {}

	/**
	 * Handle GET method. Logout user, remove the user from the session and redirect to the home page
	 * @see HttpServlet#doGet(HttpServletRequest req, HttpServletResponse resp)
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if (req.getSession().getAttribute("userSession") != null) {
			req.getSession().setAttribute("userSession", null);
		}
		resp.sendRedirect(req.getContextPath());
	}

}
