package fr.epita.iam.web;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;
import javax.servlet.ServletContextEvent;

import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import fr.epita.iam.iamlog.IamLogManager;
import fr.epita.iam.iamlog.IamLogger;
import fr.epita.iamcore.datamodel.Identity;
import fr.epita.iamcore.datamodel.User;
import fr.epita.iamcore.services.dao.IdentityDAO;
import fr.epita.iamcore.services.dao.UserDAO;

/**
 * This class handle app initialization
 * It helps to do what we need before servlet initialization
 */
public class AppListener extends ContextLoaderListener {
	private static IamLogger LOGGER = IamLogManager.getIamLogger(AppListener.class);

	@Inject
	private UserDAO userDao;

	@Inject
	private IdentityDAO identityDao;

	/**
	 * Populate database on app initialization
	 * @see org.springframework.web.context.ContextLoaderListener@contextInitialized(ServletContextEvent event)
	 */
	@Override
	public void contextInitialized(ServletContextEvent event) {
		super.contextInitialized(event);
		SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
		// Add a user
		User user = new User("user", "password");
		userDao.save(user);
		// Add some identities
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		try {
			date = sdf.parse("01/01/1970");
		} catch (ParseException e) {

		}
		Identity first = new Identity("John", "Doe", "john.doe@domain.tld", date);
		Identity second = new Identity("Emmy", "Doe", "emmy.doe@domain.tld", date);
		Identity third = new Identity("Sherlock", "Holmes", "john.doe@domain.tld", date);
		identityDao.save(first);
		identityDao.save(second);
		identityDao.save(third);
	}
}
