package fr.epita.iam.web.actions;

import javax.inject.Inject;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import fr.epita.iam.iamlog.IamLogManager;
import fr.epita.iam.iamlog.IamLogger;
import fr.epita.iamcore.datamodel.Identity;
import fr.epita.iamcore.services.dao.IdentityDAO;
import fr.epita.iamcore.services.dao.UserDAO;

/**
 * AbstractServlet is a superclass for our app's servlets
 * It sets some class member injection to suits our need.
 * Also, it defines init method, for injection.
 *
 */
public abstract class AbstractServlet extends HttpServlet {
	private static final long serialVersionUID = 3564906256288907293L;
	@SuppressWarnings("unused")
	private static IamLogger LOGGER = IamLogManager.getIamLogger(AbstractServlet.class);

	@Inject
	protected IdentityDAO dao;

	@Inject
	protected UserDAO userDao;

	/**
	 * Init servlet config to allow injection
	 * @see javax.servlet.http.HttpServlet#init((ServletConfig config) throws ServletException
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
		super.init(config);
	}

	/**
	 * Return an identity by its id
	 * @param parameter the string version of id
	 * @return identity Identity the identity which id correspond to parameter
	 * @throws ParameterException if parameter is null, empty or if any indenity correspond to the parameter
	 */
	protected Identity getIdentity(String parameter) throws ParameterException {
		if (parameter == null)
			throw new ParameterException("Parameter is null");
		if (parameter.equals(""))
			throw new ParameterException("Parameter is empty");
		try {
			Long id = new Long(parameter);
			Identity identity = dao.getById(id);
			if (identity == null)
				throw new ParameterException("Parameter does not correspond to any Identity");
			return identity;
		} catch (Exception e) {
			throw new ParameterException(e.getMessage());
		}
	}
}
